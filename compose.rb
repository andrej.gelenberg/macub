require 'psych'

class Services
  def initialize(compose, &block)
    @compose = compose
    instance_eval &block
  end

  def method_missing(name, &block)
    @compose.add_service name, &block
  end
end

class Service
  attr_reader :name

  def initialize(name, &block)
    @name = name
    @ports = []
    @links = []

    instance_eval &block
  end

  def image(img)
    @image = img
  end

  def link(name)
    @links << name
  end

  def port(from, to = nil)
    to = from if to.nil?
    @ports << "#{from}:#{to}"
  end
end

class Compose
  def verison(v = nil)
    if not v.nil?
      @version = v
    end
    @version
  end

  def initialize(&block)
    @version = 3
    @services = Hash.new

    instance_eval &block
  end

  def services(&block)
    Services.new self, &block   
  end

  def add_service(name, &block)
    @services[name] = Service.new name, &block
  end

  def get_service(name)
    @service[name]
  end

  def to_yaml
    Psych::No
  end
end
