#!/usr/bin/ruby

require_relative 'compose'

$kubernetes_version = 'v1.14.0'
etcd_version = '3.2.26'
 
def img(name, version = $kubernetes_version)
  image "gcr.io/google-containers/#{name}:#{version}"
end

c = Compose.new do
  services do

    etcd do
      img "etcd-amd64", etcd_version
      port 2379
    end

    api do
      img "kube-apiserver-amd64"
      link :etcd
    end

  end
end

c.to_yaml
